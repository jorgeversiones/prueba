import 'package:flutter/material.dart';

const iaColor1 = Color(0xFF39458E);
const iaColor2 = Color(0xFF890000);
const iaColor3 = Color(0xFFC1C1C1);
const iaColor5 = Color(0x00C1C1C1);
const iaColor4 = Color(0xFF212121);
const appNombre = 'Revisiones INVENT';

const int catalogo_marcasPartes = 15;
const int catalogo_modelosLlanta = 35;
const int catalogo_llantas = 17;
