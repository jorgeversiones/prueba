import 'package:flutter/material.dart';
import 'package:helloworld/constantes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Excelente',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: colorMat39,
        primaryColor: iaColor1,
      ),
      // A widget which will be started on application startup
      home: MyHomePage(title: appNombre),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;
  final _formaKey = GlobalKey<FormState>();

  MyHomePage({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // The title text which will be shown on the action bar
          title: Text(title),
        ),
        drawer: null,
        body: SafeArea(
            child: Stack(fit: StackFit.expand, children: [
          forma()
        ])));
  }

  ///
  Widget forma() {
    return Container(
      margin: EdgeInsets.all(8.0),
      padding: EdgeInsets.all(8.0),
      child: SingleChildScrollView(
        child: Form(
          key: _formaKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              logo(),
              txtUsuario('Usuario', 'Indique el usuario'),
              txtContra(),
              //txtServidor(),
              btnEnviar(),
            ],
          ),
        ),
      ),
    );
  }

  ///
  Widget logo() {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      SizedBox(
          height: 155.0,
          child: Image.asset(
            "assets/images/logo.png",
            fit: BoxFit.contain,
            height: 100,
            width: 100,
          ))
    ]);
  }

  ///
  Widget txtUsuario(String label, String mensajeValidacion) {
    return TextFormField(
      keyboardType: TextInputType.text,
//      onSaved: (String val) {
      //      _usuario = val;
      //  },
      // validator: (valor) {
      // if (valor.isEmpty) {
      // return mensajeValidacion;
//        }
      //      return null;
      //  },
      decoration: InputDecoration(
        labelText: label,
      ),
      initialValue: 'Usuario',
    );
  }

  ///
  Widget txtContra() {
    return TextFormField(
      keyboardType: TextInputType.text,
      obscureText: true,
//      onSaved: (String val) {
      //      _contra = val;
      //  },
//      validator: (value) {
      //      if (value.isEmpty) {
      //      return 'Indique la contraseña';
      //  }
      //return null;
//      },
      decoration: InputDecoration(
        labelText: 'Contraseña',
      ),
      initialValue: '',
    );
  }

  ///
  Widget btnEnviar() {
    return ElevatedButton(
      style: TextButton.styleFrom(primary: Colors.white, backgroundColor: iaColor1, onSurface: Colors.grey),
      child: Text('Enviar',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          )),
      onPressed: null,
    );
  }
}

///
Map<int, Color> color89 = {
  50: Color.fromRGBO(137, 0, 0, .1),
  100: Color.fromRGBO(137, 0, 0, .2),
  200: Color.fromRGBO(137, 0, 0, .3),
  300: Color.fromRGBO(137, 0, 0, .4),
  400: Color.fromRGBO(137, 0, 0, .5),
  500: Color.fromRGBO(137, 0, 0, .6),
  600: Color.fromRGBO(137, 0, 0, .7),
  700: Color.fromRGBO(137, 0, 0, .8),
  800: Color.fromRGBO(137, 0, 0, .9),
  900: Color.fromRGBO(137, 0, 0, 1),
};
MaterialColor colorMat89 = MaterialColor(0xFF890000, color89);
Map<int, Color> color39 = {
  50: Color.fromRGBO(59, 69, 142, .1),
  100: Color.fromRGBO(59, 69, 142, .2),
  200: Color.fromRGBO(59, 69, 142, .3),
  300: Color.fromRGBO(59, 69, 142, .4),
  400: Color.fromRGBO(59, 69, 142, .5),
  500: Color.fromRGBO(59, 69, 142, .6),
  600: Color.fromRGBO(59, 69, 142, .7),
  700: Color.fromRGBO(59, 69, 142, .8),
  800: Color.fromRGBO(59, 69, 142, .9),
  900: Color.fromRGBO(59, 69, 142, 1),
};
MaterialColor colorMat39 = MaterialColor(0xFF890000, color89);
// fin clase
